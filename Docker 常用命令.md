# Docker 常用命令
本部分主要有以下内容
> docker 安装

> docker 主要命令

> docker 的debug

> 组内的docker使用文档
## docker 安装
卸载历史版本
```
sudo apt-get remove docker docker-engine docker.io containerd runc
```
安装必要的软件
```
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release
```
添加docker官方的GPG 秘钥
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```
配置稳定的仓库
```
echo   "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
安装docker engine
```
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
拉取进行，执行程序
```
sudo docker run hello-world
sudo dockerd --debug
```
结果示例：

![](image/doce.png)

- 如果运行出错，尝试以下命令:
```
systemctl enable docker # 开机自动启动docker

systemctl start docker # 启动docker
systemctl restart docker # 重启dokcer
```

### 镜像加速
国内从 Docker Hub 拉取镜像有时会遇到困难，此时可以配置镜像加速器。

请首先执行以下命令，查看是否在 docker.service 文件中配置过镜像地址。
```shell
systemctl cat docker | grep '\-\-registry\-mirror'
```
如果该命令有输出，那么请执行 $ systemctl cat docker 查看 ExecStart= 出现的位置，修改对应的文件内容去掉 --registry-mirror 参数及其值，并按接下来的步骤进行配置。

如果以上命令没有任何输出，那么就可以在 /etc/docker/daemon.json 中写入如下内容（如果文件不存在请新建该文件）：
```json
{
  "registry-mirrors": [
    "https://hub-mirror.c.163.com",
    "https://mirror.baidubce.com"
  ]
}
```
注意，一定要保证该文件符合 json 规范，否则 Docker 将不能启动。
之后重新启动服务。
```
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
```
参考：https://yeasy.gitbook.io/docker_practice/install/mirror

## docker 主要命令

查看docker 的基本命令
```
docker help
docker version
docker info
docker images
```
![](image/1ge.png)
```
docker search ubuntu
docker pull ubuntu
docker run -p 8080:80 -d nginx
```
![](image/2ge.png)

访问网页

![](image/3ge.png)

查看正在运行的容器
`docker ps`

列出所有的容器
`docker ps -a `

![](image/4ge.png)

### 启动，停止，重启和杀死容器
```
docker stop my_nginx
docker start my_nginx
docker restart my_nginx
docker kill my_nginxDocker Exe
```
### docker 运行
`docker run --name my_ubuntu -it ubuntu:latest bash`

![](image/5ge.png)

`docker run -it -d --name my_ubuntu_2 ubuntu:latest bash`

`docker attach my_ubuntu_2`

![](image/6ge.png)

直接向docker容器运行命令

`docker exec -it my_ubuntu ls`

![](image/7ge.png)
### 使用 docker cp 将文件从本地复制到容器
可使用 docker cp 命令在本地计算机和创建容器间移动文件，此方法可用于覆盖配置文件。

启动NGINX

使用创建的本地文件覆盖容器 index.html 文件：
```html
<html>
<Header><title>My Website</title></header>
<body>
Hello world
</body>
</html>
```
使用创建的本地文件覆盖容器 index.html 文件：


```
$ docker cp index.html hardcore_torvalds:usr/share/nginx/html/
```
如果再次检查http://localhost:8080，应该可以看到问候语“Hello world”。

![](image/8ge.png)

### 删除 Docker 容器和镜像
```
docker rm 4e042d76c391 75b28b720835 a19c770b8621 616
docker rmi 117d060587a3 5e8b97a2a082 113a43faa138 c
```
参考：https://www.infoq.cn/article/kbtrc719-r6ghops3cr8
## docker 的debug
可以使用pycharm配置docker环境

参考： https://www.jetbrains.com/help/pycharm/using-docker-as-a-remote-interpreter.html

## 组内的docker使用文档
- [Running ray microbenchmark](https://gitee.com/endedgecloudteam/ray/wikis/Ray%20Testing%20and%20Benchmark?sort_id=2836408)
- [Docker上的Ray启动流程](https://gitee.com/endedgecloudteam/ray/wikis/Docker%E4%B8%8A%E7%9A%84Ray%E5%90%AF%E5%8A%A8%E6%B5%81%E7%A8%8B?sort_id=3226569)
- [制作支持GPU的YOLOv3+Ray的ARM64版docker镜像](https://gitee.com/endedgecloudteam/ray/wikis/%E5%88%B6%E4%BD%9C%E6%94%AF%E6%8C%81GPU%E7%9A%84YOLOv3+Ray%E7%9A%84ARM64%E7%89%88docker%E9%95%9C%E5%83%8F?sort_id=3230090)