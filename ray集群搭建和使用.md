# ray集群搭建和使用

## 在传统服务器上使用ray集群
```
ray start --head --redis-port=6379
```
会在终端显示redis的address，类似于123.45.67.89:6379

在worker节点上使用用：
1. 启动
```
 ray start --address=<address>
```

2. 关闭
```
ray stop
```
使用ray集群：
在ray集群的节点机器上，使用ray.init(address="auto")
或者使用ray.init(address=...)

## 在10.101.16.28，10.101.16.27上测试本地集群的使用
在10.101.16.28的机器上运行，在python脚本中使用 ray.init(address="10.101.16.28:6379")或者ray.init(address="auto")结果是一样的。

![](image/11953.png)

在10.101.16.27上加入该节点中，`ray start --address='10.101.16.28:6379' --redis-password='5241590000000000'`

![](image/ie123.png)

此时返回2个ip，说明27的服务器已经加入进集群了。

> 如果遇到`RuntimeError: Unable to connect to Redis.`的问题

![](image/5123.png)

进一步查明是redis无法连接
![](image/45251.png)

则在head节点上设置iptables
```
sudo iptables -F
```
就可以顺利加入ray集群了。
## 使用kubernetes部署集群（Ray Cluster Launcher方式）
kubernetes可以看做是分布式ray的底座，每个ray node对应一个kubernetes的pod，ray根据对资源的需求增加，删除kubernetes的pods。

ray 集群有一个head节点和一组worker节点组成。

![](https://docs.ray.io/en/master/_images/ray-cluster.jpg)


如果需要一个 Ray 集群，那么一共需要两个角色的节点，分别是 Head 和 Worker。在 Kubernetes 上部署，可以参考 [Deploying on Kubernetes](https://docs.ray.io/en/master/cluster/kubernetes.html)。

首先需要有一个kubernetes环境.

1. 创建conda环境ray-zhh，在环境中使用python=3.7, 并且安装kubernetes，ray
```
pip install kubernetes
pip install -U ray
```
2. 使用 ` kubectl get pods` 查看当前是否能够检测到kubernetes集群。
3. 启动集群
```
# Create or update the cluster. When the command finishes, it will print
# out the command that can be used to get a remote shell into the head node.
$ ray up ray/python/ray/autoscaler/kubernetes/example-full.yaml

# List the pods running in the cluster. You shoud only see one head node
# until you start running an application, at which point worker nodes
# should be started. Don't forget to include the Ray namespace in your
# 'kubectl' commands ('ray' by default).
$ kubectl -n ray get pods

# Get a remote screen on the head node.
$ ray attach ray/python/ray/autoscaler/kubernetes/example-full.yaml
$ # Try running a Ray program with 'ray.init(address="auto")'.

# Tear down the cluster
$ ray down ray/python/ray/autoscaler/kubernetes/example-full.yaml
```
ray 集群部署完毕

## 使用ray集群运行程序
在`ray/python/ray/autoscaler/kubernetes/example-full.yaml`中有对services进行配置，可以使用services对外与集群进行交互。使用ray集群的方法有3种，推荐使用第三种。

### 1. 使用kubernetes的job方式执行程序
通过创建kubernetes的job，在yaml中配置job的程序，就可以执行程序了。
```
$ kubectl create -f ray/doc/kubernetes/job-example.yaml
```
程序运行的结果为：
```
$ kubectl -n ray get pods
NAME                               READY   STATUS    RESTARTS   AGE
example-cluster-ray-head-rpqfb     1/1     Running   0          11m
example-cluster-ray-worker-4c7cn   1/1     Running   0          11m
example-cluster-ray-worker-zvglb   1/1     Running   0          11m
ray-test-job-8x2pm-77lb5           1/1     Running   0          8s

# Fetch the logs. You should see repeated output for 10 iterations and then
# 'Success!'
$ kubectl -n ray logs ray-test-job-8x2pm-77lb5
```
运行后清理kubernetes中的job的方法：
```
# List Jobs run in the Ray namespace.
$ kubectl -n ray get jobs
NAME                 COMPLETIONS   DURATION   AGE
ray-test-job-kw5gn   1/1           10s        30s

# Delete the finished Job.
$ kubectl -n ray delete job ray-test-job-kw5gn

# Verify that the Job's pod was cleaned up.
$ kubectl -n ray get pods
NAME                               READY   STATUS    RESTARTS   AGE
example-cluster-ray-head-rpqfb     1/1     Running   0          11m
example-cluster-ray-worker-4c7cn   1/1     Running   0          11m
example-cluster-ray-worker-zvglb   1/1     Running   0          11m
```


### 2. 通过开启port-forward的方法services对外提供接口（没成功）
该方法在服务器上没有成功，应该是ray现阶段提供的服务有些问题。

使用如下命令开通对外接口：
```
$ kubectl -n ray port-forward service/example-cluster-ray-head 10001:10001
```
然后开启一个新的终端，输入如下命令运行程序：
```
$ python ray/doc/kubernetes/example_scripts/run_local_example.py
```

## 3. 在ray head上运行程序，让他自行分配任务（我觉得比较好）
1. 将程序拷贝到head节点上：
```
# Copy the test script onto the head node.
$ kubectl -n ray cp ray/doc/kubernetes/example_scripts/run_on_head.py example-cluster-ray-head-p9mfh:/home/ray
```
执行程序：
```
# Run the example program on the head node.
$ kubectl -n ray exec example-cluster-ray-head-p9mfh -- python /home/ray/run_on_head.py
# You should see repeated output for 10 iterations and then 'Success!'
```
或者进入head节点运行：
```
# Copy the test script onto the head node.
$ kubectl -n ray cp ray/doc/kubernetes/example_scripts/run_on_head.py example-cluster-ray-head-p9mfh:/home/ray

# Run the example program on the head node.
$ kubectl -n ray exec example-cluster-ray-head-p9mfh -- python /home/ray/run_on_head.py
# You should see repeated output for 10 iterations and then 'Success!'
```

- 如果要让集群对外提供dashboard，也需要使用port-forward方法暴露接口:
```
$ kubectl -n ray port-forward service/example-cluster-ray-head 8265:8265
```
这样就可以在浏览器中使用`http://localhost:8265`访问。
## ray cluster launcher 命令
[命令列表](https://docs.ray.io/en/master/cluster/commands.html)
- ray up xxx.yaml
辅助的命令：--no-restart ，--no-config-cache ，--restart-only 等
- ray down xxx.yaml
- ray submit直接运行脚本
- ray attach xxx.yaml进入集群中
- ray rsync-up/down 上传下载文件到集群head节点
- ray dashboard xxx.yaml 监控集群状态
- ray monitor xxx.yaml 监控集群状态(使用ray cluster launcher启动集群使用)
- kubectl logs xxx 监控集群状态(使用kubernetes operator启动集群使用)

## python中命令autoscaler集群
```
>>> # Request 1000 CPUs.
>>> request_resources(num_cpus=1000)
>>> # Request 64 CPUs and also fit a 1-GPU/4-CPU task.
>>> request_resources(num_cpus=64, bundles=[{"GPU": 1, "CPU": 4}])
>>> # Same as requesting num_cpus=3.
>>> request_resources(bundles=[{"CPU": 1}, {"CPU": 1}, {"CPU": 1}])
```

## 以10.5.26.178服务器为例，展示搭建过程
### 创建ray集群
1. 创建conda环境，并且进入conda环境：

![](image/41044.png)

2. 查看安装kubernetes， ray：

![](image/1253.png)

3. 使用`ray up ray/python/ray/autoscaler/kubernetes/example-full.yaml`命令启动集群

4. 查看集群是否已经启动：

![](image/41601.png)
若看到以上有3个po，则说明部署了一个有1个head，2个worker的ray集群。

###  使用集群运行程序
因为配置文件ray/python/ray/autoscaler/kubernetes/example-full.yaml中已经配置了services服务：

![](image/65311.png)
所以在搭建集群的时候，就已经有services了，可以通过命令查看
```
kubectl -n ray get services
```

![](image/5444.png)

1. 使用job方式运行程序
```
kubectl create -f ray/doc/kubernetes/job-example.yaml
```

![](image/123123.png)

上面命令行的意思是：先创建作业，查看po状态，发现运行错误，查看运行的错误信息，然后修改错误的地方，删除此次错误的job。

job执行的的内容是运行job_example.py这个python脚本，出现错误的原因是无法找到该文件。

![](image/70805.png)

为了解决无法下载github文件的问题，我把ray的project克隆到我的gitee私人账号上，将url修改为gitee地址：

![](image/1851.png)

再尝试运行：

![](image/72118.png)

![](image/172432.png)

可以看到，程序执行完成。

上面截图显示了：运行job，查看po的状态，运行完成后看logs输出，最后删除job清理资源。

2. 在head node上运行程序
确定存在ray集群，并且找到ray的head的pod
```
kubectl -n ray get po
```

![](image/291411.png)

使用`kubectl cp`拷贝文件到head节点
```
kubectl -n ray cp ray/doc/kubernetes/example_scripts/run_on_head.py example-cluster-ray-head-shxher-ray-head-shxhk:/home/ray
```
进入head的pod：
```
kubectl -n ray exec -it example-cluster-ray-head-shxhk -- bash
```
执行程序：
```
python run_on_head.py
```
运行结果为：

![](image/123e.png)

成功，上面程序有把任务分发到3个pods。
## 附注
- ray基于ubuntu的镜像：
https://hub.docker.com/r/rayproject/ray

- 如果要使用基于docker的ray-gpu版本，需要安装NVIDIA的docker runtime：
https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker
 
 - 使用YAML配置的一些属性列表：https://docs.ray.io/en/master/cluster/config.html#examples

