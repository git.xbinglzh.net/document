# Windows10安装GPU版

## 总体流程

>1. 安装驱动器NVIDIA
>2. 安装CUDA
>3. 安装cudnn
>4. pytorch 安装

###  安装驱动器NVIDIA
查看电脑中的显卡的型号
![gpu-type](image/Image.png)

在安装CUDA之前首先更新一下[Nvidia驱动](https://www.nvidia.cn/geforce/drivers/)，选择对应的显卡就可以了：
![nvidia](https://pic1.zhimg.com/v2-754f50a2a3debadc98210c50cb258158_r.jpg)


### 安装CUDA
安装CUDA之前，需要安装 [visual studio](https://my.visualstudio.com/Downloads?q=visual%20studio&pgroup=)：
![vs](https://pic2.zhimg.com/80/v2-df047193d46e4018afd7deb9fd5730e9_720w.jpg)

安装好后，可以通过[英伟达官网给出的支持CUDA的GPU清单](https://developer.nvidia.com/zh-cn/cuda-gpus)检查自检查自己的显卡是否支持CUDA，在[CUDA下载页面](https://developer.nvidia.com/zh-cn/cuda-gpus)选择合适的CUDA版本，这里我们选择当下Pytorch支持的最新的CUDA11.1，安装类型选exe[local]，点击右下角的Download(2.8GB)，最后下载下来
![CUDA](https://pic2.zhimg.com/80/v2-f7d3d3a40b3e5ab6b5aa09e7e7f94a21_720w.jpg)

安装好了CUDA以后，CMD命令行窗口输入以下指令可以检查安装的CUDA版本：
```nvcc -v ```
![test](https://pic4.zhimg.com/80/v2-bfc4e00401bea6eb0624d162a4cdb88f_720w.jpg)
编译成功了以后，去以下文件夹通过cmd运行deviceQuery.exe：
![cuda-test](https://pic2.zhimg.com/80/v2-b17749f179b02ffda1b6125c4f6195fd_720w.jpg)
最后显示Result = PASS，这个测试就算通过了。

### 安装cudnn
在[cudnn下载页面](https://developer.nvidia.com/zh-cn/cudnn)下载不同版本的cudnn，这里需要你注册加入NVIDIA才能下载cudnn。
![cudnn](https://pic2.zhimg.com/80/v2-f2bd0c9a8b60774d7eca7f052a03b6ed_720w.jpg)

按照[cuDNN官方安装教程]，解压了cuDNN压缩文件，得到bin，include，lib

系统环境变量里检查以下变量是否存在：
![path](https://pic3.zhimg.com/80/v2-f172740ba39b661bc95dbbeea13e92ba_720w.jpg)

### pytorch 安装
创建conda环境
![envs](https://pic2.zhimg.com/80/v2-f5452749b74d64ba8d14f9b4cf572255_720w.jpg)

设置代理：
![proxy](image/643.png)
查看镜像源

conda config –show

然后输入

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/

conda config --set show_channel_urls yes

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/pytorch/

去[pytorch官网](https://pytorch.org/get-started/previous-versions/)下载匹配cudnn的包
![pytorch](image/2914.png)

## 出现的问题
报错：
Key already registered with the same priority: GroupSpatialSoftma
![registered](image/e.png)
原因，原始的torch没卸载干净，有重复的软件包，删除文件夹重新安装torch
解决网址（https://discuss.pytorch.org/t/importerror-key-already-registered-with-the-same-priority/86271/6）

报错caffe2_detectron_ops.dll 无法找到
解决方法：
![dll](image/1e.png)

## 安装成功&测试：
![final](image/557.png)
安装好CUDA后，就可以使用visual studio写C++代码，或者PYCHARM写python代码