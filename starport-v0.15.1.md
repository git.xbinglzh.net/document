
## cosmos生态
tendermint为core， cosmos进行外围的交互，使用Starport快速部署。
Tendermint Core，Cosmos SDK 和跨链通信协议（IBC）是构建「区块链互联网」必不可少的基础层。但是，为了促使生态进一步发展和成熟，Tendermint 认识到需要更高级别的工具来支持在强大的 Cosmos 堆栈上更快更简单的应用开发。因此，Tendermint 开发创建了 Starport 工具，帮助新开发人员在数分钟内搭建，建模并启动 PoS 区块链。

## 安装
安装go>=1.16


1. 下载
`wget https://golang.google.cn/dl/go1.16.5.linux-amd64.tar.gz --no-check-certificate`

2. Extract the archive you downloaded into /usr/local, creating a Go tree in /usr/local/go.
Important: This step will remove a previous installation at /usr/local/go, if any, prior to extracting. Please back up any data before proceeding.
For example, run the following as root or through sudo:

`rm -rf /usr/local/go && tar -C /usr/local -xzf go1.16.5.linux-amd64.tar.gz`

3. Add /usr/local/go/bin to the PATH environment variable.
You can do this by adding the following line to your $HOME/.profile or /etc/profile (for a system-wide installation):

`export PATH=$PATH:/usr/local/go/bin`

Note: Changes made to a profile file may not apply until the next time you log into your computer. To apply the changes immediately, just run the shell commands directly or execute them from the profile using a command such as source $HOME/.profile.
	
4. Verify that you've installed Go by opening a command prompt and typing the following command:

`$ go version`



------------------------------

安装特定版本的starport：

`curl https://get.starport.network/starport@v0.15.1! | bash`

原因：V0.16.X版本的没有vue的前端界面和rest API

如果无法脚本下载，就下载压缩包，进行解压手动安装。
```
starport app github.com/username/voter

starport serve

starport type poll title options
```

参考：https://tutorials.cosmos.network/voter/
> 注意：现阶段无法在window上安装

## 机器选择
1. 使用10,的本地服务器图形IDEA编辑太卡了
2. 使用之江云服务器无法查看前端

3. 还是使用vmware，本地机器搭建环境学习tutorial，idea打开的不费劲

## rest API
前端rest接口，数据的流程：

To write anything to a blockchain or perform any other state transition, a client makes an HTTP POST request. In our case, the voter web app is the client.

The POST request with a title and options goes to the http://localhost:1317/voter/poll 
(opens new window)
endpoint handler that is defined in x/voter/client/rest/txPoll.go.

The handler creates an unsigned transaction that contains an array of messages. The client then signs the transaction and sends it to http://localhost:1317/txs 
(opens new window)
. The application processes the transaction by sending each message to a corresponding handler, in our case x/voter/handler.go.

A handler then calls a CreatePoll function that is defined in x/voter/keeper/poll.go that writes the poll data into the store.


## front end要求
需要安装一下版本：

[zhenghuanhuan@ ~]$ node --version

v12.22.1

[zhenghuanhuan@ ~]$ npm --version

6.14.12

使用npm 用代理，否则npm下载很慢，且出错。
```
--registry=https://registry.npm.taobao.org
npm i @vue/cli-service --registry=https://registry.npm.taobao.org
```

出现npm ERR! XXXX
解决方法：

```
mv node_modules/ /root/temp
npm cache verify
npm install --cache /tmp/empty-cache --registry=https://registry.npm.taobao.org
npm fund
npm run serve
```
## V0.16.X版本安装成功界面

![](image/12qw.png)
> 注意：以下使用IP访问，无法连接tendermint core

正确姿势：

查看前端，注意必选用google浏览器

![](image/234e.png)
前端结构解析：

![](image/58264.png)


## starport的配置
修改配置文件
(base) [root@ ~/.planet/config]# vim config.toml

![](image/345ge.png)
> 注意：官网说在这里修改配置可以取消创建交易为空的区块，但是我在机器上实验，该设置没有生效，原因还未找到。



## 其他：


### 如果端口被占用
listen tcp 0.0.0.0:4500: bind: address already in use
Waiting for a fix before retrying...

![](image/qwere.png)

查看端口,关闭该端口，重新启动。

[root@ ~]# lsof -i:4500

COMMAND    PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME

starport 63437 root    7u  IPv6 817841      0t0  TCP *:4500 (LISTEN)

[root@ ~]# pkill starport

[root@ ~]# lsof -i:4500





### go设置代理：
go env -w GOPROXY=https://goproxy.cn,direct

参考教程：
https://www.chainnews.com/articles/175556306428.htm

### 版本npm与nodejs不适配


On ubuntu 18.04 following steps saved me
To remove the distro-stable version, type the following:

`sudo apt remove nodejs`

This command will remove the package and retain the configuration files.

`sudo apt purge nodejs`

As a final step, you can remove any unused packages that were automatically installed with the removed package:

`sudo apt autoremove`

Refresh your local package index by typing:

`sudo apt update`

Enable the NodeSource repository by running the following command
(optinal) If you need to install another version, for example 14.x, just change setup_12.x with setup_14.x

`curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -`

Once the NodeSource repository is enabled, install Node.js and npm by typing:

`sudo apt install nodejs`

Verify that the Node.js and npm were successfully installed by printing their versions:
```
node --version (v12.18.4)
npm --version (6.14.6)
```

### curl: (60) SSL certificate problem: unable to get local issuer certificate 错误

到网址下载cacert.pem证书

https://curl.se/docs/caextract.html

下载成功后，放到板子上，我是放在这个路径下了

/etc/curlssl/cacert.pem

然后配置到环境变量

export CURL_CA_BUNDLE=/etc/curlssl/cacert.pem

然后访问下百度：

curl -v https://www.baidu.com