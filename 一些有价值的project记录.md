# 一些有价值的project记录

- ray 支持 Fairseq library的分布式训练，[Fairseq library](https://github.com/pytorch/fairseq)是基于pytorch的序列型模型的toolkit，包括翻译，归纳摘要，语言模型和文本生成类型的任务。
在project中，复现了一些2020,2021论文的代码。也提供一些论文的预训练和预处理的模型。


- Facebook开发的检测分割的与训练框架[detectron2](https://github.com/facebookresearch/detectron2)：panoptic segmentation, Densepose, Cascade R-CNN, rotated bounding boxes, PointRend, DeepLab, etc. 
有model zoo,是一个library，可以基于此进行research，训练更快，有完整的doc文档。

 - [Gradient-checkpointing](https://github.com/cybertronai/gradient-checkpointing):GPU 内存太小可能是神经网络训练过程中最大的拦路虎。OpenAI 推出的 gradient-checkpointing 工具程序包，对于前馈模型来说，仅仅需要增加 20% 的计算时间，就能让 GPU 处理十倍大的模型。这个工具包的开发者是 OpenAI 的研究科学家 Tim Salimans 和前 Google Brain 工程师的数据科学家 Yaroslav Bulatov 。![](https://github.com/cybertronai/gradient-checkpointing/blob/master/img/sqrtn.png)
    感觉是个黑科技，技术能力比较强的项目。



 ## 强化学习
 - uber发布的一些论文的强化学习分布式实现 [uber](https://github.com/uber-research/deep-neuroevolution)。包括2篇文章的实现。基于OpenAI.
    1. [Deep Neuroevolution: Genetic Algorithms Are a Competitive Alternative for Training Deep Neural Networks for Reinforcement Learning](https://arxiv.org/abs/1712.06567)

    2. [Improving Exploration in Evolution Strategies for Deep Reinforcement Learning via a Population of Novelty-Seeking Agents](https://arxiv.org/abs/1712.06560)

-   

## 比赛
54万奖金+百度地图海量业务数据集，智能交通CV赛题来了 