# ray测试
官方的测试用例比较全，也有记录测试结果。

## 1. clusterbenchmark
```
ray clusterbenchmark
```
结果：

latency to get an 1G object over network 3.43 +- 0.55

## 2. microbenchmark
主要测试
1. Measure the time required to submit a remote task to the scheduler.
2. Measure the time required to submit a remote task to the scheduler  (where the remote task returns one value).
3. Measure the time required to submit a remote task to the scheduler and get the result.
4. Measure the time required to submit a remote task to the scheduler and put the result.
5. Measure the time required to do dot.
```
ray microbenchmark
```
结果：

single client get calls per second 8579.53 +- 270.55
single client put calls per second 3777.0 +- 48.72
single client put gigabytes per second 4.55 +- 0.27
multi client put calls per second 7416.44 +- 40.0
multi client put gigabytes per second 13.88 +- 1.48
single client tasks sync per second 1242.73 +- 50.76
single client tasks async per second 11138.99 +- 234.85
multi client tasks async per second 23679.09 +- 190.0
1:1 actor calls sync per second 2267.69 +- 103.23
1:1 actor calls async per second 7066.36 +- 35.73
1:1 actor calls concurrent per second 5924.31 +- 138.56
1:n actor calls async per second 8981.23 +- 106.2
n:n actor calls async per second 26761.04 +- 471.35
2021-04-21 15:29:31,378	WARNING worker.py:1072 -- WARNING: 48 PYTHON workers have been started. This could be a result of using a large number of actors, or it could be a consequence of using nested tasks (see https://github.com/ray-project/ray/issues/3644) for some a discussion of workarounds.
n:n actor calls with arg async per second 8232.72 +- 125.9

官方测试结果：
https://github.com/ray-project/ray/tree/master/release/release_logs

ray的dashboard：

![](image/52748.png)


> ray的使用中，可以根据程序需要声明memory和CPU,GPU资源，也可以使用autoscaling功能扩展节点。