# ray 的安装
## ray on server 部署文档

在服务器上直接安装ray:
```
conda create --name ray
conda activate ray
conda install --name ray pip
pip install ray
```
测试样例：

![](image/139.png)

## ray on docker 部署文档

```
docker pull rayproject/ray-ml
```
测试镜像是否安装成功：
`docker run --shm-size=2G -t -i rayproject/ray`

![](image/428.png)



## 当前版本的ray环境是开发版本，无法运行通过ray的测试用例
![](image/753.png)